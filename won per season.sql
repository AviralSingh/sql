-- This query counts the number of wins for each team in each season.

-- Select the season, winner, and count of winner (number of wins) for each season and team.
SELECT season, winner, COUNT(winner)

-- Group the results by season and winner to count the number of wins for each team in each season.
FROM matches

-- Group the results by season and winner.
GROUP BY season, winner

-- Order the results by season.
ORDER BY season;
