-- This query retrieves the count of umpires from each country, excluding those from India.
-- Select the country and count of each country.
SELECT u.country, count(u.country)
FROM umpires u
-- Join the umpires table with a subquery that combines umpire1 and umpire2 columns from the matches table.
-- This subquery creates a list of umpires from the matches table.
INNER JOIN (
    SELECT umpire1 AS umpire FROM matches
    UNION
    SELECT umpire2 AS umpire FROM matches
) as t
ON t.umpire = u.umpire
-- Filter out umpires from India.
WHERE u.country != ' India'
-- Group the results by country.
GROUP BY u.country;
