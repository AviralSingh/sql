-- This query counts the number of matches played in each season.

-- Select the season and count the number of matches for each season.
SELECT season, count(*)
FROM matches

-- Group the results by season to count matches for each season.
GROUP BY season;
