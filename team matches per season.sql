-- This query counts the total number of teams in each season based on the teams participating in matches.

-- Select the season, team1, and calculate the total_teams as the sum of counts for team1 and team2 in each season.
SELECT season, team1, COUNT(team1) + COUNT(team2) as total_teams
FROM matches

-- Group the results by season and team1, considering all possible groupings using ROLLUP.
GROUP BY ROLLUP (season, team1)

-- Order the results by season.
ORDER BY season;
