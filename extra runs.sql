-- This query calculates the total extra runs conceded by each bowling team in the 2016 season.
-- Select the bowling team name and calculate the total extra runs.
SELECT d.bowling_team, sum(d.extra_runs)
FROM deliveries d
-- Join the "deliveries" table with the "matches" table on the match_id.
INNER JOIN matches m 
    ON d.match_id = m.id
-- Filter the data to include only matches from the 2016 season.
WHERE m.season = 2016
-- Group the results by bowling team to calculate the total extra runs for each team.
GROUP BY d.bowling_team;
