-- This query calculates the economy rate for bowlers in the 2015 season based on the provided formula.
-- Select the bowler's name and calculate the economy using the given formula.
SELECT d.bowler, 
(sum(d.total_runs) - sum(d.legbye_runs) - sum(d.bye_runs)) / (count(d.ball) / 6) as economy
FROM deliveries d
-- Join the "deliveries" table with the "matches" table on the match_id.
INNER JOIN matches m ON d.match_id = m.id
-- Filter the data to include only matches from the 2015 season.
WHERE m.season = 2015
-- Group the results by bowler to calculate their individual economy.
GROUP BY d.bowler
-- Order the results in ascending order based on the economy rate.
ORDER BY economy
-- Limit the results to the top 10 bowlers with the lowest economy rates.
LIMIT 10;
