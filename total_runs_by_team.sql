-- This query calculates the total runs scored by each batting team.

-- Select the batting_team and sum of total_runs for each team.
SELECT batting_team, SUM(total_runs) as "total runs"
FROM deliveries

-- Group the results by batting_team to aggregate the total runs for each team.
GROUP BY batting_team;
