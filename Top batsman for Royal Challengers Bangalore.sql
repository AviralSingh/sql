-- This query selects the top 10 batsmen from the "Royal Challengers Bangalore" team based on their total runs scored.
-- Select the batsman's name and calculate the sum of their runs (batsman_runs).
SELECT batsman, sum(batsman_runs)
FROM deliveries
-- Filter the data to include only rows where the "batting_team" is 'Royal Challengers Bangalore.'
WHERE batting_team like 'Royal Challengers Bangalore'
-- Group the results by batsman to aggregate their total runs.
GROUP BY batsman
-- Order the results in descending order based on the total runs scored.
ORDER BY sum(batsman_runs) DESC
-- Limit the results to the top 10 batsmen.
LIMIT 10;
